# Dubin slide template

## Description
Create Gitlab hosted [revealjs](https://revealjs.com/) [Dubin themed](https://gitlab.com/lassegs/dubin) slides from simple markdown. 


## Install
No install, everything happens in the GitLab pipes.

Fork this repo and edit [slides/slides.md](/-/blob/main/slides/slides.md)

Check Gitlab Deployments --> Pages. It tells you which URL your presentation is served at. Should be similar to [https://lassegs.gitlab.io/dubintemplate/](https://lassegs.gitlab.io/dubintemplate/)

## Usage

[Fork this repo](https://gitlab.com/lassegs/dubintemplate/-/forks/new).

Write markdown in [slides/slides.md](/-/blob/main/slides/slides.md) to generate content for your slides. Markdown is not documented here, see [elsewhere](https://www.markdownguide.org/getting-started). More info on Revealjs flavored markdown can be found here: [revealjs.com/markdown](https://revealjs.com/markdown/)

### Separators
Separators for horizontal slides are 3 dashes `--- `

Separators for horizontal slides are 2 dashes `-- `

Separators for notes are newline Note: `^Note:`

## Example: 

```
# Title 

---

Horizontal slide

--

Vertical slide 

Note:
Helpful reminders hidden in notes.

---

## subheading
more content

Note:
This content is only visible in speaker mode by pressing **'s'**.


```


