<!-- .slide: data-background="https://upload.wikimedia.org/wikipedia/commons/thumb/9/93/Eccc-logo-web-purpose-desktop.svg/640px-Eccc-logo-web-purpose-desktop.svg.png" data-background-opacity="1" data-background-size="30%" data-background-position="95% 5%" -->


# ECCC's Approach to AI Security

Nora SIG AI Security

---


<div class="thirdsplit">	
                <div class="gridleft">
                <h2>About me</h2>
                <ul>
                        <li>Lasse Gullvåg Sætre</li>
                        <li>Enabling Technologies</li>
                        <li><a mailto:"lgs@forskningsradet.no">lgs@forskningsradet.no</a></li>
                    </ul>
                </div>
                <div class="gridright">
                    <img src="https://www.forskningsradet.no/siteassets/illustrasjoner/portefoljemuliggjorende-teknologier.svg">
                </div>
            </div>


---

## About you

![](https://cdn.masto.host/oslotown/cache/media_attachments/files/112/237/306/205/996/593/original/8c45d81106ec87f5.jpeg)

---

<!-- .slide: data-autoslide="2000" data-background-video="https://anti-brands.fra1.digitaloceanspaces.com/brands/forskningsradet/media/NFR_STINGER_PICTURE_16-9.mp4" -->


---

<!-- .slide: data-background="https://www.enisa.europa.eu/++theme++enisa-www-theme/templates/img/logo-with-text-big-20.webp" data-background-opacity="1" data-background-size="20%" data-background-position="95% 5%" -->



**Impact of Emerging Technologies:**
<small>Quantum computing and artificial intelligence (AI) emerge as key factors impacting the threat landscape. While these technologies offer significant opportunities, they also introduce vulnerabilities that malicious actors may exploit.</small>

**AI-Related Threats Gain Likelihood:**
<small>"Abuse of AI" and "AI Disrupting/Enhancing Cyber Attacks" have gained likelihood. This aligns with the increasing coverage of AI applications at scale and the ethical considerations surrounding AI use. Concerns about public over-reliance on AI and the potential blind spots introduced by probabilistic threat detection methods are highlighted.</small>

<small>Source: [ENISA Foresight report 2024](https://www.enisa.europa.eu/topics/foresight) </small>

---

<!-- .slide: data-background="https://i.imgur.com/ImDaVVL.png" data-background-size="contain" -->

![]()

---

![](https://i.imgur.com/K8EivxQ.png)

Note:
The Competence Centre has the **overall objective of promoting research, innovation and deployment in the area of cybersecurity**.


---

<!-- .slide: data-background="https://upload.wikimedia.org/wikipedia/commons/thumb/9/93/Eccc-logo-web-purpose-desktop.svg/640px-Eccc-logo-web-purpose-desktop.svg.png" data-background-opacity="1" data-background-size="30%" data-background-position="95% 5%" -->


## Strategic goals

<small>

1. By 2027, the ECCC and the Network will have funded European SMEs in developing and using strategic cybersecurity technologies, services and processes through a coordinated cascade funding mechanism via NCCs and national co-financing that lowers the application threshold for SMEs.
2. By 2027, the ECCC and the Network will have supported and grown the cybersecurity
professional workforce in both quantity and quality through the standardisation and
certification of cybersecurity skills and investments in education and training of cybersecurity professionals.
3. By 2027, the ECCC and the Network will have strengthened the research, development and innovation expertise and competitiveness of the EU cybersecurity community through the development and implementation of an efficient and coherent action plan.

Source: [ECCC Strategic Agenda](https://cybersecurity-centre.europa.eu/strategic-agenda_en)
</small>

---

## Closed calls

<small>

- [Explainable and Robust AI (AI Data and Robotics Partnership) (RIA)](https://ec.europa.eu/info/funding-tenders/opportunities/portal/screen/opportunities/topic-details/horizon-cl4-2024-human-01-06)
- [Novel applications of AI and other enabling technologies for security operation centres](https://ec.europa.eu/info/funding-tenders/opportunities/portal/screen/opportunities/topic-details/digital-eccc-2024-deploy-cyber-06-enablingtech)
- [Enhancing tools and capabilities to fight advanced forms of cyber threats and cyber-dependent crimes](https://ec.europa.eu/info/funding-tenders/opportunities/portal/screen/opportunities/topic-details/horizon-cl3-2023-fct-01-06)

</small>
---

## Ongoing and Forthcoming calls

<small>

- [Pilot action for the establishment of future Union Testing Facilities in AI](https://ec.europa.eu/info/funding-tenders/opportunities/portal/screen/opportunities/topic-details/digital-2024-ai-act-06-testai)
- [AI regulatory sandboxes: EU-level coordination and support](https://ec.europa.eu/info/funding-tenders/opportunities/portal/screen/opportunities/topic-details/digital-2024-ai-act-06-sandbox)
- [Approaches and tools for security in software and hardware development and assessment](https://ec.europa.eu/info/funding-tenders/opportunities/portal/screen/opportunities/topic-details/horizon-cl3-2024-cs-01-01)
- [Mitigating new threats and adapting investigation strategies in the era of Internet of Things](https://ec.europa.eu/info/funding-tenders/opportunities/portal/screen/opportunities/topic-details/horizon-cl3-2024-fct-01-01)

</small>

---

## Planned calls

<small>WIP - taken from ECCC Draft action plan. Calls scheduled for 25/26.</small>

- **AI4CS**: Development and Implementation of AI-driven Cybersecurity Solutions
- **AI4CS2**: Methods and Tools Supporting the use of AI in cybersecurity Processes
- **SecureAI**: Enhancing The Security and Robustness of AI Models and Systems

---

<!-- .slide: data-background-video="https://anti-brands.fra1.digitaloceanspaces.com/brands/forskningsradet/media/NFR_ENDPOSTER-16-9_LIME_SHAPE.mp4" data-background-opacity="1"-->